package cn.airfei;

import cn.airfei.service.IWelcomeService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

import java.util.Arrays;

/**
 * @description:
 * @author: air
 * @create: 2020-03-13 11:30
 */
public class Entrance {

	public static void main(String[] args) {
		System.out.println("args = " + "hello world");

		String xmlPath="//air/java/cn-airfei/spring-framework-5.2.4.RELEASE/srpingdemo/src/main/resources/spring/spring-config.xml";
		ApplicationContext applicationContext=new FileSystemXmlApplicationContext(xmlPath);

		IWelcomeService welcomeService= (IWelcomeService) applicationContext.getBean("welcomeService");
		welcomeService.sayHello("强大的spring 框架");

	}

}
