package cn.airfei.service.impl;

import cn.airfei.service.IWelcomeService;

/**
 * @description:
 * @author: air
 * @create: 2020-03-13 11:33
 */
public class WelcomeServiceImpl implements IWelcomeService {

	@Override
	public String sayHello(String name) {
		System.out.println("name = " + name);
		return name;
	}
}
