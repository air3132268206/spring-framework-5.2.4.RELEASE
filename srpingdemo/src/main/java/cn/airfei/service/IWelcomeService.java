package cn.airfei.service;

/**
 * @description:
 * @author: air
 * @create: 2020-03-13 11:31
 */
public interface IWelcomeService {
	String sayHello(String name);
}
